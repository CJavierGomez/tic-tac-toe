# TicTacToe

Documentación de la aplicación TicTacToe.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.4.

## Observaciones

Para reproducir este proyecto se deben de cumplir los siguientes requisitos:

- Contar con un IDE (Preferiblemente Visual Studio Code)
- Tener instalado Node.js
- Y contar con Angular CLI
  En el siguiente enlace encontraran los requisitos que se deben cumplir para poder trabajar con Angular 10:
  `https://dev.to/sandymarmolejo/instalacion-de-angular-10-17e1`

## Instrucciones

Para ejecutar este proyecto se deben seguir los siguientes pasos:

- Descargar el código.
- Abrir el codigo en Visual Studio Code
- Abrir la consola de comandos y ejecutar `npm install`
- Ejecutar `npm serve`
- En el navegador ir a `http://localhost:4200/`.
