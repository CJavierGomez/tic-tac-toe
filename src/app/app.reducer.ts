import { ActionReducerMap } from '@ngrx/store';
import * as loading from './pages/loading.reducer';
import * as mode from './pages/components/mode.reducer';
import * as game from './pages/components/game/game.reducer';

export interface AppState {
	ui: loading.State;
	mode: mode.State;
	game: game.State;
}

export const appReducers: ActionReducerMap<AppState> = {
	ui: loading.loadingReducer,
	mode: mode.modeReducer,
	game: game.gameReducer
};
