import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { appRoutingProviders, routing } from './app.routing';
import { AppComponent } from './app.component';

//Para trabajar con Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';

// Modulos Creados
import { SharedModule } from './shared/shared.module';
import { PagesModule } from './pages/pages.module';

// NgRx
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers } from './app.reducer';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		SharedModule,
		PagesModule,
		routing,
		ReactiveFormsModule,
		AngularFireModule.initializeApp(environment.firebaseConfig),
		AngularFirestoreModule, // imports firebase/firestore, only needed for database features
		AngularFireDatabaseModule,
		StoreModule.forRoot(appReducers),
		// Instrumentation must be imported after importing StoreModule (config is optional)
		StoreDevtoolsModule.instrument({
			maxAge: 25, // Retains last 25 states
			logOnly: environment.production // Restrict extension to log-only mode
		})
	],
	providers: [appRoutingProviders],
	bootstrap: [AppComponent]
})
export class AppModule {}
