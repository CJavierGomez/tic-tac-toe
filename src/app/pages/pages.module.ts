import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Componentes
import { HomeComponent } from './components/home/home.component';
import { GamesListComponent } from './components/games-list/games-list.component';
import { GameComponent } from './components/game/game.component';
import { PaperComponent } from './components/game/paper/paper.component';
import { SpaceComponent } from './components/game/space/space.component';

@NgModule({
	declarations: [HomeComponent, GamesListComponent, GameComponent, PaperComponent, SpaceComponent],
	imports: [CommonModule, PagesRoutingModule, ReactiveFormsModule, FormsModule]
})
export class PagesModule {}
