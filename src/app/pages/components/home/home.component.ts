import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// NgRx
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as modeActions from '../mode.actions';
import * as ui from '../../loading.actions';
import * as gameActions from '../game/game.actions';

// Services
import { GameService } from '../../services/game.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	title: string;
	loading: boolean = false; // Variable para saber cuando se están cargando los datos.
	private ngUnsubscribe: Subject<any> = new Subject<any>(); // Observable para desubscribir todos los observables

	constructor(private gameService: GameService, private router: Router, private store: Store<AppState>) {}

	ngOnInit(): void {
		this.title = '¡Bienvenido!';
		this.store.dispatch(modeActions.deleteMode());
		this.store.dispatch(gameActions.setGame({ game: null }));
		this.store
			.select('ui')
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe((ui) => (this.loading = ui.isLoading));
	}

	/**
	 * Método para crear una nueva sala de juego.
	 */
	createGame() {
		this.store.dispatch(ui.isLoading());
		this.gameService.addGame().then((data) => {
			this.store.dispatch(modeActions.ownerMode());
			this.store.dispatch(ui.stopLoading());
			this.router.navigate(['/game', data.id]);
		});
	}
}
