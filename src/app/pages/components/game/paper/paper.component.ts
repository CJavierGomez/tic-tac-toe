import { Component, OnInit } from '@angular/core';

// NgRx
import { Store } from '@ngrx/store';
import { Paper } from 'src/app/pages/models/paper.model';
import { AppState } from '../../../../app.reducer';

@Component({
	selector: 'app-paper',
	templateUrl: './paper.component.html',
	styleUrls: ['./paper.component.css']
})
export class PaperComponent implements OnInit {
	paper: Paper;

	constructor(private store: Store<AppState>) {
		this.store.select('game').subscribe((game) => {
			this.paper = game.game?.paper;
		});
	}

	ngOnInit(): void {}
}
