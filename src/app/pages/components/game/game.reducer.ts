import { createReducer, on } from '@ngrx/store';
import { setGame } from './game.actions';

// Models
import { Game } from '../../models/game.model';

export interface State {
	game: Game;
}

export const initialState: State = {
	game: null
};

const _gameReducer = createReducer(
	initialState,

	on(setGame, (state, { game }) => ({ ...state, game: game }))
);

export function gameReducer(state, action) {
	return _gameReducer(state, action);
}
