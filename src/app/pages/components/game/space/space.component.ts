import { Component, Input, OnInit } from '@angular/core';

// NgRx
import { Store } from '@ngrx/store';
import { AppState } from '../../../../app.reducer';
import * as gameActions from '../game.actions';

// Models
import { Game } from '../../../models/game.model';

// Services
import { PaperService } from '../../../services/paper.service';
import { GameService } from '../../../services/game.service';

@Component({
	selector: 'app-space',
	templateUrl: './space.component.html',
	styleUrls: ['./space.component.css']
})
export class SpaceComponent implements OnInit {
	@Input() space: any;

	game: Game;
	mode: string;

	constructor(private store: Store<AppState>, private paperService: PaperService, private gameService: GameService) {
		this.store.subscribe((state) => {
			this.game = state.game.game;
			this.mode = state.mode.mode;
		});
	}

	ngOnInit() {}

	/**
	 * Método para actualizar la información del juego y de board una vez se selecciona un espacio en la board.
	 * Se usa la desestructuración de objetos para poder manipular objetos y no romper las reglas del patrón Redux
	 */
	spaceClicked() {
		const paper = { ...this.game.paper };
		if (paper.isGameRunning && this.space.state === null) {
			let board: any[] = [];
			paper.board.forEach((spaceBoard) => {
				const spaceTemp = { ...spaceBoard };
				if (spaceTemp.id === this.space.id) {
					spaceTemp.state = this.game.activePlayer;
				}

				board.push(spaceTemp);
			});

			const gameTemp: Game = this.paperService.changePlayerTurn({ ...this.game, paper: { ...paper, board: board } });
			this.gameService.updateGame(gameTemp).then((data) => {
				this.store.dispatch(gameActions.setGame({ game: gameTemp }));
			});
		}
	}
}
