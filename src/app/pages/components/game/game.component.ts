import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// Models
import { Game } from '../../models/game.model';

// NgRx
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.reducer';
import * as ui from '../../loading.actions';
import * as game from './game.actions';

// Services
import { GameService } from '../../services/game.service';
import { PaperService } from '../../services/paper.service';

@Component({
	selector: 'app-game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
	public game: Game;
	loading: boolean;
	mode: string;
	editing: boolean = false;
	ownerName: string;
	guestName: string;
	private ngUnsubscribe: Subject<any> = new Subject<any>(); // Observable para desubscribir todos los observables

	constructor(
		private gameService: GameService,
		private route: ActivatedRoute,
		private store: Store<AppState>,
		private paperService: PaperService
	) {
		this.store.pipe(takeUntil(this.ngUnsubscribe)).subscribe((state) => {
			this.loading = state.ui.isLoading;
			this.mode = state.mode.mode;
		});
	}

	ngOnInit(): void {
		this.store.dispatch(ui.isLoading());
		this.getGame();
	}

	/**
	 * Método para obtener la información del juego por medio del id obtenido por la url.
	 */
	getGame(): void {
		this.route.params.forEach((params: Params) => {
			const id = params['id'];
			this.gameService
				.getGame(id)
				.pipe(takeUntil(this.ngUnsubscribe))
				.subscribe((gameFire) => {
					this.game = gameFire;
					this.ownerName = this.game.owner;
					this.guestName = this.game.guest;
					this.store.dispatch(game.setGame({ game: this.game }));
					if (this.mode === 'guest' && !this.game.isFull) {
						this.changeIsfull();
					}
					this.store.dispatch(ui.stopLoading());
				});
		});
	}

	/**
	 * Método para iniciar una nueva partida.
	 */
	startGame() {
		const paper = this.paperService.newPaper();
		this.gameService.updateGame({ ...this.game, paper: paper }).then((newGame) => {
			this.store.dispatch(game.setGame({ game: { ...this.game, paper: paper } }));
		});
	}

	/**
	 * Método para actualizar los nombres de los jugadores.
	 */
	actualizarName() {
		this.gameService.updateGame({ ...this.game, owner: this.ownerName, guest: this.guestName }).then(() => (this.editing = false));
	}

	/**
	 * Método para cambiar el estado de la sala a llena.
	 * Esto se hace una vez cuando entra el segundo jugador a la sala
	 */
	changeIsfull() {
		this.gameService.updateGame({ ...this.game, isFull: true }).then((data) => {
			this.store.dispatch(game.setGame({ game: { ...this.game, isFull: true } }));
		});
	}

	/**
	 * Este metodo se ejecuta cuando el componente se destruye
	 * Usamos este método para cancelar todos los observables.
	 */
	ngOnDestroy(): void {
		// End all subscriptions listening to ngUnsubscribe
		// to avoid memory leaks.
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
	}
}
