import { createAction, props } from '@ngrx/store';

// Models
import { Game } from '../../models/game.model';

export const setGame = createAction('[Game Component] setGame', props<{ game: Game }>());
