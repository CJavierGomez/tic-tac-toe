import { createReducer, on } from '@ngrx/store';
import * as modeActions from './mode.actions';

export interface State {
	mode: string;
}

export const initialState: State = {
	mode: null
};

const _modeReducer = createReducer(
	initialState,
	on(modeActions.ownerMode, (state) => ({ ...state, mode: 'owner' })),
	on(modeActions.guestMode, (state) => ({ ...state, mode: 'guest' })),
	on(modeActions.deleteMode, (state) => ({ ...state, mode: null }))
);

export function modeReducer(state, action) {
	return _modeReducer(state, action);
}
