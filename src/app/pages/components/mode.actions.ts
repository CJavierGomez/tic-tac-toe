import { createAction } from '@ngrx/store';

export const ownerMode = createAction('[Mode Component] OwnerMode');
export const guestMode = createAction('[Mode Component] GuestMode');
export const deleteMode = createAction('[Mode Component] DeleteMode');
