import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// NgRx
import { Store } from '@ngrx/store';
import * as ui from '../../loading.actions';
import { guestMode } from '../mode.actions';
import { AppState } from '../../../app.reducer';

// Models
import { Game } from '../../models/game.model';

// Services
import { GameService } from '../../services/game.service';

@Component({
	selector: 'app-games-list',
	templateUrl: './games-list.component.html',
	styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit, OnDestroy {
	loading: boolean = false; // Variable para saber cuando se están cargando los datos.
	games: Game[] = []; // Variable para almacenar los juegos existentes.

	formSearch: FormGroup;

	private ngUnsubscribe: Subject<any> = new Subject<any>(); // Observable para desubscribir todos los observables

	constructor(private fb: FormBuilder, private gameService: GameService, private store: Store<AppState>) {
		this.formSearch = this.fb.group({
			id: ['', [Validators.required]]
		});
	}

	ngOnInit(): void {
		this.store.dispatch(guestMode());
		this.store
			.select('ui')
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe((ui) => (this.loading = ui.isLoading));
		this.getAllGames();
	}

	/**
	 * Método para obtener todos los juegos almacenados en Firebase.
	 */
	getAllGames() {
		this.store.dispatch(ui.isLoading());
		this.gameService
			.getGames()
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe((games) => {
				this.games = games;
				this.store.dispatch(ui.stopLoading());
			});
	}

	/**
	 * Método que se ejecuta cuando se busca una sala por ID.
	 */
	search() {
		if (this.formSearch.invalid) {
			this.getAllGames();
			return;
		}
		this.store.dispatch(ui.isLoading());
		this.gameService
			.getGame(this.formSearch.value.id)
			.pipe(takeUntil(this.ngUnsubscribe))
			.subscribe((game) => {
				this.games = [];
				if (game) {
					this.games.push(game);
				}
				this.store.dispatch(ui.stopLoading());
			});
	}

	/**
	 * Este metodo se ejecuta cuando el componente se destruye
	 * Usamos este método para cancelar todos los observables.
	 */
	ngOnDestroy(): void {
		// End all subscriptions listening to ngUnsubscribe
		// to avoid memory leaks.
		this.ngUnsubscribe.next();
		this.ngUnsubscribe.complete();
	}
}
