import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

// NgRx
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';

@Injectable({
	providedIn: 'root'
})
export class GameGuard implements CanActivate {
	state: AppState = null;

	constructor(private store: Store<AppState>, private router: Router) {
		this.store.subscribe((state) => {
			this.state = state;
		});
	}

	canActivate(): boolean {
		// Si la sala está llena o el modo del juegador no existe lo envía al home.
		if (this.state.game.game?.isFull || !this.state.mode.mode) {
			this.router.navigate(['/home']);
			return false;
		} else {
			return true;
		}
	}
}
