import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Models
import { Game } from '../models/game.model';

@Injectable({
	providedIn: 'root'
})
export class GameService {
	private gamesCollection: AngularFirestoreCollection<Game>;
	private gameDoc: AngularFirestoreDocument<Game>;
	private games: Observable<Game[]>;
	private game: Observable<Game>;

	constructor(private afs: AngularFirestore) {}

	/**
	 * Método para obtener todas los juegos o salas almacenadas en la base de datos de firebase.
	 */
	getGames(): Observable<Game[]> {
		this.gamesCollection = this.afs.collection<Game>('Games');
		return (this.games = this.gamesCollection.snapshotChanges().pipe(
			map((changes) => {
				return changes.map((action) => {
					const data = action.payload.doc.data() as Game;
					data.gameId = action.payload.doc.id;
					return data;
				});
			})
		));
	}

	/**
	 * Método para agregar una nueva sala a la base de datos.
	 */
	addGame() {
		this.gamesCollection = this.afs.collection<Game>('Games');
		return this.gamesCollection.add({
			owner: 'Jugador 1',
			guest: 'Jugador 2',
			activePlayer: 'X',
			isFull: false,
			ownerWinners: 0,
			paper: {
				board: [],
				paperSize: 9,
				turnCount: 0,
				isGameRunning: false,
				isGameOver: false,
				winner: false
			},
			guestWinners: 0
		});
	}

	/**
	 * Método para obtener un juego especifico de Firebase.
	 * @param id id del juego a obtener
	 */
	getGame(id: string): Observable<Game> {
		this.gameDoc = this.afs.doc<Game>(`Games/${id}`); // Ruta de la Game en particular en firebase.
		return (this.game = this.gameDoc.snapshotChanges().pipe(
			map((action) => {
				if (action.payload.exists == false) {
					return null;
				} else {
					const data = action.payload.data() as Game;
					data.gameId = action.payload.id;
					return data;
				}
			})
		));
	}

	/**
	 * Método para actualizar la informaciòn de un juego en Firebase.
	 * @param game juego a actualizar.
	 */
	updateGame(game: Game): Promise<void> {
		const idGame = game.gameId;
		delete game.gameId; // Le borramos el id al juego para cuando lo vuelva a guardar no lo incluya dentro de sus atributos actualizados.
		return this.afs
			.collection<Game>('Games')
			.doc(idGame)
			.set({ ...game, paper: JSON.parse(JSON.stringify(game.paper)) });
	}

	/**
	 * Método para borrar a un juego de la base de datos de firebase.
	 * @param id id del juego a eliminar
	 */
	deleteGame(id: string): Promise<void> {
		this.gameDoc = this.afs.doc<Game>(`Games/${id}`);
		return this.gameDoc.delete();
	}
}
