import { Injectable } from '@angular/core';

// Models
import { Game } from '../models/game.model';
import { Paper } from '../models/paper.model';

@Injectable({
	providedIn: 'root'
})
export class PaperService {
	constructor() {}

	/**
	 * Método para instanciar un nuevo paper correspondiente a un juego.
	 */
	newPaper(): Paper {
		let newPaper = new Paper([], 9, 0, true, false, false);
		newPaper.board = this.createBoard();
		return newPaper;
	}

	/**
	 * Método para instanciar la board o tablero del paper.
	 */
	createBoard() {
		let board = [];
		for (let i = 0; i < 9; i++) {
			board.push({ id: i, state: null });
		}
		return board;
	}

	/**
	 * Método para actualizar la información del juego una vez un jugador realice una jugada.
	 * @param game Juego a actualizar.
	 */
	changePlayerTurn(game: Game) {
		let gameTemp = this.winnerBoard(game);
		if (!gameTemp.paper.isGameOver) gameTemp.activePlayer = gameTemp.activePlayer === 'X' ? 'O' : 'X';
		gameTemp.paper.turnCount++;
		gameTemp.paper.isGameOver = gameTemp.paper.isGameOver ? true : false;

		return gameTemp;
	}

	/**
	 * Método para actualizar un juego en caso de victoria.
	 * @param game juego a actualizar.
	 */
	winnerBoard(game: Game) {
		if (this.isWinner(game)) {
			game.paper.winner = true;
			game.paper.isGameRunning = false;
			game.paper.isGameOver = true;
			if (game.activePlayer === 'X') {
				game.ownerWinners++;
			} else {
				game.guestWinners++;
			}
		}
		return game;
	}

	/**
	 * Método para saber si hubo un ganador
	 * @param game juego a consultar si existe un ganador.
	 */
	isWinner(game: Game): boolean {
		return this.checkDiag(game.paper.board) || this.checkRows(game.paper.board, 'row') || this.checkRows(game.paper.board, 'col')
			? true
			: false;
	}

	/**
	 * Método para verificar si en las filas o las columnas hubo un ganador.
	 * @param board board con la información de cada uno de los campos.
	 * @param mode modo a verificar (col o row) columnas o filas.
	 */
	checkRows(board, mode): boolean {
		const ROW = mode === 'row' ? true : false,
			DIST = ROW ? 1 : 3,
			INC = ROW ? 3 : 1,
			NUMTIMES = ROW ? 7 : 3;

		for (let i = 0; i < NUMTIMES; i += INC) {
			let firstSquare = board[i].state,
				secondSquare = board[i + DIST].state,
				thirdSquare = board[i + DIST * 2].state;

			if (firstSquare && secondSquare && thirdSquare) {
				if (firstSquare === secondSquare && secondSquare === thirdSquare) return true;
			}
		}
		return false;
	}

	/**
	 * Método para verificar si la diagonal hubo un ganador.
	 * @param board board o tablero con la información a verificar.
	 */
	checkDiag(board) {
		const timesRun = 2,
			midSquare = board[4].state;

		for (let i = 0; i <= timesRun; i += 2) {
			let upperCorner = board[i].state,
				lowerCorner = board[8 - i].state;

			if (midSquare && upperCorner && lowerCorner) {
				if (midSquare === upperCorner && upperCorner === lowerCorner) return true;
			}
		}

		return false;
	}
}
