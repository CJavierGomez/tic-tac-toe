export class Paper {
	constructor(
		public board: any[],
		public paperSize: number,
		public turnCount: number,
		public isGameRunning: boolean,
		public isGameOver: boolean,
		public winner: boolean
	) {}
}
