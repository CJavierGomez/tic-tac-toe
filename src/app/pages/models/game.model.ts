import { Paper } from './paper.model';
export class Game {
	constructor(
		public owner: string,
		public guest: string,
		public isFull: boolean,
		public ownerWinners: number,
		public guestWinners: number,
		public paper: Paper,
		public activePlayer: string,
		public gameId?: string
	) {}
}
