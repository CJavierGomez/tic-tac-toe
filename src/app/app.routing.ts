import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'pages', pathMatch: 'full' },
	{ path: 'pages', loadChildren: () => import('./pages/pages.module').then((m) => m.PagesModule) }
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<Route> = RouterModule.forRoot(appRoutes);
